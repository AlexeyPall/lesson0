
import matplotlib.pyplot as plt
import numpy as np

from SpeechProcess import SpeechProcess
from SpeechPlot import SpeechPlot


BASE_PATH = '/home/alex/lessons/ASR/decoder/test_data/data/myDaNet_etalon_test/etalon/'


def get_wav_files_path(prefix, first_num, last_num):
    """
    """

    files_path = []

    for i in range(first_num, last_num+1):
        files_path.append(BASE_PATH + prefix + str(i).zfill(2) + '.wav')

    return files_path


def read_etalon_files(num_yes_etalons=5, num_no_etalons=5, abs_transform=False):
    """ Read Etalon files
    """

    ''' Initialize lists with file paths to wav file'''
    etalons = dict()

    ''' Fill etalons with path to wav files '''
    # etalons['yes'] = get_wav_files_path('da_', 1, num_yes_etalons)
    etalons['yes'] = [BASE_PATH + 'da_01.wav',
                      BASE_PATH + 'da_02.wav',
                      BASE_PATH + 'da_03.wav',
                      BASE_PATH + 'da_05.wav']
    etalons['no'] = get_wav_files_path('net_', 2, num_no_etalons)

    ''' Initialize lists with SpeechProcess objects '''
    speech_etalon = {'yes': [], 'no': []}

    ''' Read wav files '''
    for _etalon_type, _files in etalons.items():
        for _wav_file in _files:
            speech_etalon[_etalon_type].append(SpeechProcess(_wav_file, abs_transform=abs_transform))

    return speech_etalon


def plot_entropy(speech_etalon, type_etalon, figure_num):

    color = 'bgrcmy'

    for _speech, _cl in zip(speech_etalon[type_etalon], color):
        SpeechPlot.plot_entropy_frames(_speech.frames_entropy, figure_num,
                                       speech_color=_cl, need_to_show=False)
        print('Speech :' + type_etalon + ': with color :'
              + _cl + ': threshold : ' + str(_speech.threshold_entropy))

    plt.title('Entripy for etalons :' + type_etalon + ':')


def calc_diff(speech1, speech2):

    sub_speech = (speech1 - speech2) # / len(speech1)

    mult_diff = np.multiply(sub_speech, sub_speech)
    # mult_diff = np.multiply(mult_diff, mult_diff)

    err = sum(mult_diff)

    # err /= len(sub_speech)

    return err


def compare_speech_with_offset(speech1_obj, speech2_obj, max_offset, step_offset):
    """
    """
    speech1, speech2 = speech1_obj.get_speech(), speech2_obj.get_speech()

    if len(speech1) > len(speech2):
        speech1 = speech1[:len(speech2)]

    ''' length of speech 1 should be always less length of speech 2 '''
    # if len(speech1) > len(speech2):
    #     speech1, speech2 = speech2, speech1

    diff_speech = calc_diff(speech1, speech2[:len(speech1)])

    end_speech1 = len(speech1)

    for speech_offset in range(step_offset, max_offset, step_offset):

        start_pos_sp2 = speech_offset

        end_pos_sp2 = start_pos_sp2 + len(speech1)

        ''' if length of speech1 + speech offset > length of speech2 '''
        if end_pos_sp2 > len(speech2):
            end_speech1 = len(speech1) - (end_pos_sp2 - len(speech2))
            end_pos_sp2 = len(speech2)

        cur_diff = calc_diff(speech1[:end_speech1], speech2[start_pos_sp2:end_pos_sp2])

        diff_speech = min(diff_speech, cur_diff)

    return diff_speech


def recognizer(filename_to_test, filename_etalon, abs_transform=False):
    """
    """

    min_diff_YES = 0
    min_diff_NO = 0

    speech_to_test = SpeechProcess(filename_to_test, abs_transform=abs_transform)

    for i in range(len(filename_etalon['yes'])):
        min_diff_YES += min(compare_speech_with_offset(filename_etalon['yes'][i], speech_to_test, 200, 1),
                            compare_speech_with_offset(speech_to_test, filename_etalon['yes'][i], 200, 1))

    for i in range(len(filename_etalon['no'])):
        min_diff_NO += min(compare_speech_with_offset(filename_etalon['no'][i], speech_to_test, 200, 1),
                           compare_speech_with_offset(speech_to_test, filename_etalon['no'][i], 200, 1))

    # print('File : ' + filename_to_test.split('/')[-1])

    if min_diff_YES < min_diff_NO:
        # print('\tDA')
        return 'DA'
    else:
        # print('\tNO')
        return 'NO'


from multiprocessing import Pool
from functools import partial

threads_count = 8

need_to_abs_origin_audio_signal = True

def recognize_da_net():
    speech_etalon = read_etalon_files(5, 5, abs_transform=need_to_abs_origin_audio_signal)

    filename_to_test = dict()

    filename_to_test['yes'] = get_wav_files_path('../test/da_', 6, 19)
    filename_to_test['no'] = get_wav_files_path('../test/net_', 6, 21)

    p = Pool(threads_count)

    file__to_test = []
    print('----------- Test with YES -------------')
    for _file in filename_to_test['yes']:
        file__to_test.append(_file)
        # recognizer(_file, speech_etalon, abs_transform=True)

    print(p.map(partial(recognizer, filename_etalon=speech_etalon, abs_transform=need_to_abs_origin_audio_signal),
                file__to_test))

    print('----------- Test with NO -------------')
    file__to_test = []
    for _file in filename_to_test['no']:
        file__to_test.append(_file)
        # recognizer(_file, speech_etalon, abs_transform=True)

    print(p.map(partial(recognizer, filename_etalon=speech_etalon, abs_transform=need_to_abs_origin_audio_signal),
                file__to_test))


#----------------------- DIGIT recognition ---------------------------

BASE_PATH_DIGIT = '/home/alex/lessons/ASR/decoder/test_data/data/myDigits_etalon_test/etalon/'


def recognizer_digits(filename_to_test, etalon_digits, abs_transform=False):
    """
    """

    err = [0 for i in range(len(etalon_digits))]

    speech_to_test = SpeechProcess(filename_to_test, abs_transform=abs_transform)

    for _key in etalon_digits:
        err[_key] = min(compare_speech_with_offset(etalon_digits[_key], speech_to_test, 200, 1),
                        compare_speech_with_offset(speech_to_test, etalon_digits[_key], 200, 1))

    # print('recognize for file : ', filename_to_test)
    # print(err)
    # print(err.index(min(err)))

    return err.index(min(err))



def read_etalon_digits():

    filenames = []

    for _idx in range(10):
        filenames.append(BASE_PATH_DIGIT + str(_idx) + '_1.wav')

    filename_etalon = dict()

    for _idx in range(len(filenames)):
        # print('Initialize file : ', filenames[_idx])
        filename_etalon[_idx] = SpeechProcess(filenames[_idx], abs_transform=True)

    return filename_etalon


def recognize_digits():

    etalon_digits = read_etalon_digits()

    filename_to_test = []

    for _digit in range(10):
        for _test_num in range(2, 6):
            filename_to_test.append(BASE_PATH_DIGIT + '../test/' + str(_digit) + '_' + str(_test_num) + '.wav')

    p = Pool(threads_count)

    res_arr = p.map(partial(recognizer_digits, etalon_digits=etalon_digits, abs_transform=True),
                    filename_to_test)

    for i in range(10):
        print(res_arr[4 * i], res_arr[4 * i + 1], res_arr[4 * i + 2], res_arr[4 * i + 3])



if __name__ == '__main__':

    # recognize_da_net()

    recognize_digits()


