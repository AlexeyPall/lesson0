
import numpy as np
import scipy.io.wavfile
import math

class SpeechProcess:
    """ Used to store signal with speech

    Store audio signal
    Simple processing of the signal
    """

    def __init__(self, file_name, abs_transform=False):
        """
        """

        ''' Init storage '''
        self.signal = None
        self.frames = np.array([], dtype=np.int)
        self.signal_pdf = np.array([], dtype=np.int)
        self.frames_pdf = np.array([], dtype=np.int)
        self.frames_entropy = np.array([])

        ''' VAD params '''
        self.threshold_entropy = 0
        self.COUNT_BINS = 400  # bins in PDF

        ''' Initialize frame parameters'''
        self.frame_length = 400 #400
        self.frame_offset = 200 #200

        ''' Signal params '''
        self.abs_transform = abs_transform

        ''' Read the file '''
        # print('Reading file : ' + str(file_name))
        self.read_wav_file(file_name)

    def read_wav_file(self, file_name):
        """
        """

        ''' Read wav file '''
        fs, y = scipy.io.wavfile.read(file_name)

        ''' Save signal in the class '''
        if self.abs_transform:
            self.signal = np.array(abs(y), dtype=np.int)
        else:
            self.signal = np.array(y, dtype=np.int)

        self.calc_entropy()

        return y

    def init_frames(self):
        """ Create signal's frames

        Frames with duration 25 ms [self.frame_length = 400]
        and 50% intersection [self.frame_offset = 200]
        """

        ''' Start position in the signal '''
        start_sample = 0

        ''' Calc count of frames '''
        count_frames = int(((len(self.signal) - self.frame_length) + self.frame_offset - 1) / self.frame_offset)

        ''' Initialize frames array'''
        self.frames = np.zeros((count_frames, self.frame_length), dtype=np.int)

        ''' Split the signal to frames '''
        frame_idx = 0
        while start_sample + self.frame_length < len(self.signal):

            ''' Add new frame '''
            self.frames[frame_idx] = self.signal[start_sample:start_sample + self.frame_length]

            ''' Add offset to move on the next frame '''
            start_sample += self.frame_offset
            frame_idx += 1

    def calc_entropy(self):
        """ Calc entropy for each frame

        :return: Threshold to find voice activity
        """

        ''' Split a signal on frames '''
        self.init_frames()

        ''' Initialize frames pdf '''
        self.frames_pdf = np.zeros((len(self.frames), self.COUNT_BINS))

        ''' Initialize entry array '''
        self.frames_entropy = np.zeros(len(self.frames))

        ''' Calc count of values in range for each frame to calc PDF '''
        for frame_idx in range(len(self.frames)):

            ''' Clac PDF for the frame '''
            self.frames_pdf[frame_idx], _ = np.histogram(self.frames[frame_idx]
                                                         , self.COUNT_BINS
                                                         , (min(self.signal), max(self.signal))
                                                         , density=False)

            ''' Take len of the frame to Normalize PDF '''
            len_of_frame = len(self.frames[frame_idx])

            ''' Calc entropy for the frame '''
            for frame_val in self.frames_pdf[frame_idx]:

                ''' Normalize probability '''
                probability_val = frame_val/len_of_frame

                ''' If the probability is not zero '''
                if probability_val > np.finfo(float).eps:
                    self.frames_entropy[frame_idx] += probability_val * math.log(probability_val)

            ''' mult on -1 '''
            self.frames_entropy[frame_idx] *= -1

        ''' Calc threshold entropy to get voice activity'''
        # self.threshold_entropy = max(self.frames_entropy) - max(self.frames_entropy) / 4.0
        self.threshold_entropy = 3.5

        return self.threshold_entropy

    def get_entropy_vad(self):
        """ Search voice using entropy of the frames

        :return: period [start_point, end_point] of voice in the signal
        """

        ''' periods '''
        periods = []

        ''' Initialize start frame index '''
        start_frame = -1

        for frame_idx in range(len(self.frames_entropy)):

            ''' Speech has started '''
            if self.frames_entropy[frame_idx] > self.threshold_entropy and start_frame == -1:
                start_frame = frame_idx

            ''' Speech has stopped '''
            if self.frames_entropy[frame_idx] < self.threshold_entropy and start_frame != -1:

                ''' Add found period to result '''
                periods.append([start_frame * self.frame_offset, frame_idx * self.frame_offset + self.frame_length])

                ''' Reset start_frame '''
                start_frame = -1

        # if len(periods) != 1:
        #     print('Warning : count of speech intervals = ' + str(len(periods)) + ' but should be equal 1')

        return [[periods[0][0], periods[-1][1]]]

    def get_speech(self):
        """ Return a speech signal separated by build-in VAD

        !Returns only first found speech activity!
        """

        period = self.get_entropy_vad()[0]

        start_point = period[0]
        end_point = period[1]

        return self.signal[start_point:end_point]

