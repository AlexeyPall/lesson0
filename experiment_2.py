
from SpeechProcess import SpeechProcess
from SpeechPlot import SpeechPlot

from python_speech_features import mfcc
from python_speech_features import delta
from python_speech_features import logfbank

import numpy as np
import matplotlib.pyplot as plt

from multiprocessing import Pool
from functools import partial


BASE_PATH = '/home/alex/lessons/ASR/decoder/test_data/data/myDaNet_etalon_test/etalon/'
BASE_PATH_DIGIT = '/home/alex/lessons/ASR/decoder/test_data/data/myDigits_etalon_test/etalon/'

def get_mfcc(filepath):

    speech = SpeechProcess(filepath)

    # (rate, sig) = wav.read(BASE_PATH + 'da_01.wav')
    rate = 16000
    sig = speech.get_speech()
    mfcc_feat = mfcc(sig, rate)
    d_mfcc_feat = delta(mfcc_feat, 2)
    fbank_feat = logfbank(sig, rate)

    return fbank_feat


def calc_mfcc_diff(mfcc_feat1, mfcc_feat2):

    # sub2 = mfcc_feat1[:min(len(mfcc_feat1), len(mfcc_feat2))] - mfcc_feat2[:min(len(mfcc_feat1), len(mfcc_feat2))]
    sub2 = mfcc_feat1[:12] - mfcc_feat2[:12]
    res = np.zeros(len(sub2))
    for i in range(len(sub2)):
        sub2[i] = np.multiply(sub2[i], sub2[i])
        res[i] = np.sum(sub2[i])

    return res


def calc_diff_features(fich):
    return np.sum(np.multiply(fich, fich))


def get_wav_files_path(prefix, first_num, last_num):
    """
    """

    files_path = []

    for i in range(first_num, last_num+1):
        files_path.append(BASE_PATH + prefix + str(i).zfill(2) + '.wav')

    return files_path


def recognizer(feature_to_recognize):

    etalon_yes_file = BASE_PATH + 'da_01.wav'
    etalon_no_file = BASE_PATH + 'net_01.wav'

    features_etalon_yes = get_mfcc(etalon_yes_file)
    features_etalon_no = get_mfcc(etalon_no_file)

    for _feature in feature_to_recognize:

        features_to_test = get_mfcc(_feature)

        if calc_diff_features(calc_mfcc_diff(features_etalon_yes, features_to_test)) < \
           calc_diff_features(calc_mfcc_diff(features_etalon_no, features_to_test)):
            print('DA')
        else:
            print('NET')


def take_win_digit(features_to_test_file, etalons):

    features_to_test = get_mfcc(features_to_test_file)

    min_diff = float("inf")
    win_digit = -1

    for _digit in range(len(etalons)):
        new_diff = calc_diff_features(calc_mfcc_diff(etalons[_digit], features_to_test))
        if new_diff < min_diff:
            min_diff = calc_diff_features(calc_mfcc_diff(etalons[_digit], features_to_test))
            win_digit = _digit

    return win_digit



threads_count = 8


def digit_recognizer():

    etalons = []

    for i in range(10):
        etalons.append(get_mfcc(BASE_PATH_DIGIT + str(i) + '_1.wav'))

    p = Pool(threads_count)

    for _digit_to_recognize in range(10):

        files_to_test = []

        for _test_num_digit in range(2, 6):
            files_to_test.append(BASE_PATH_DIGIT + '../test/' + str(_digit_to_recognize) +
                                 '_' + str(_test_num_digit) + '.wav')

        print(p.map(partial(take_win_digit, etalons=etalons), files_to_test))


if __name__ == '__main__':

    ''' Recognize Da/NET '''
    files_to_test_yes = get_wav_files_path('da_', 1, 5)
    for _file_to_add in get_wav_files_path('../test/da_', 6, 19):
        files_to_test_yes.append(_file_to_add)

    files_to_test_no = get_wav_files_path('net_', 1, 5)
    for _file_to_add in get_wav_files_path('../test/net_', 6, 21):
        files_to_test_no.append(_file_to_add)

    print('----- Files with DA ---------')
    recognizer(files_to_test_yes)

    print('----- Files with NET ---------')
    recognizer(files_to_test_no)

    ''' Try to recognize digits '''
    digit_recognizer()