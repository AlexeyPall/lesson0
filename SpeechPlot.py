
import matplotlib.pyplot as plt
import numpy as np

class SpeechPlot:
    """
    """

    @staticmethod
    def plot_entropy_frames(frames_entropy, figure_num=1, speech_color='b',
                            need_to_show=True, show_grid=True):
        """ Plot entropy
        """

        plt.figure(figure_num)
        plt.title('Entropy of frames in the signal')
        plt.plot(frames_entropy, speech_color)

        if show_grid:
            plt.grid()

        if need_to_show:
            plt.show()

    @staticmethod
    def plot_signal(speech_obj, figure_num=1, plot_color='b',
                    need_to_show=True, show_grid=True):

        signal_timing = [a / 16000 for a in range(len(speech_obj.signal))]

        plt.figure(figure_num)
        plt.title('Signal Wave...')
        plt.plot(signal_timing, speech_obj.signal, color=plot_color)

        if show_grid:
            plt.grid()

        if need_to_show:
            plt.show()

    @staticmethod
    def plot_speech(speech_obj, figure_num=1, speech_color='r', need_to_show=True):

        plt.figure(figure_num)
        plt.title('Signal Wave...')

        signal_timing = [a / 16000 for a in range(len(speech_obj.signal))]

        for period in speech_obj.get_entropy_vad():
            start_point = period[0]
            end_point = period[1]

            plt.plot(signal_timing[start_point:end_point],
                     speech_obj.signal[start_point:end_point],
                     color=speech_color)

        if need_to_show:
            plt.show()


    @staticmethod
    def get_start_end_frame(speech_obj):

        period = speech_obj.get_entropy_vad()[0]

        start_frame = int(period[0] / speech_obj.frame_offset)
        end_frame = int((period[1] - speech_obj.frame_length) / speech_obj.frame_offset)

        return start_frame, end_frame

    @staticmethod
    def plot_frame_dist(speech_obj, speech_diff_obj, num_frame, figure_num=1, speech_color='r', need_to_show=True):

        start_frame, end_frame = SpeechPlot.get_start_end_frame(speech_diff_obj)

        cur_frame = SpeechPlot.get_start_end_frame(speech_diff_obj)[0] + num_frame
        frame_dist = []

        for i in range(start_frame, end_frame):
            sub_frames = (speech_obj.frames[i]) - (cur_frame)
            frame_dist.append(np.sum(np.multiply(sub_frames, sub_frames)))
            # frame_dist.append(np.sum(sub_frames))

        plt.figure(figure_num)
        plt.title('Frame distance for :' + str(num_frame) + ': frame')

        plt.plot(frame_dist, speech_color)

        if need_to_show:
            plt.show()


